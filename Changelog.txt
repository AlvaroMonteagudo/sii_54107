# Changelog
All notable changes to this project will be documented in this file

##[3.0]
###Added
-Rama práctica 3
-Programa logger
-Programa bot
-Fin práctica 3

##[2.0]
###Added
-Rama práctica 2
-Creación de un fichero README para indicar las instrucciones
-Fin práctica 2

###Changed
-Añadido movimiento a la pelota y a las raquetas
-Añadido que la pelota se haga pequeña con el tiempo

##[1.3]
###Added
-Creación de Changelog
-Fin práctica 1

###Changed
-Cambio en los .h
