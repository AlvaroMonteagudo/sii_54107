#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "Puntos.h"

int main() {

int fd;
Puntos puntos;

	if(mkfifo("FIFO", 0600)<0) {
		perror("Creacion FIFO");
		return 1;
	}
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("Apertura FIFO");
		return 1;
	}
	
	while(read(fd,&puntos,sizeof(puntos))==sizeof(puntos)){
		if(puntos.ganador==1) {
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador1);
		}
		else if(puntos.ganador==2) {
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador2);
		}
	}
	close(fd);
	unlink("FIFO");
	return(0);
}
